import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http'
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class AuthProvider {

  //dataService: any
  //data: any
  urlBase: string = 'http://officeboy.com.mx:8089/v0.0.1/api/auth'
  dataObserver: any

  constructor(public http: HttpClient) { }


  login(myData): Observable<any> {
    let body = JSON.stringify(myData)
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' })

    return this.http
      .put(`${this.urlBase}/login`, body, { headers: headers })
      /*.map((res: any) => {
        console.log(res)
        this.dataObserver.next(res);
        return res
      })
      */
  }







  read(id = null): Observable<any> {
    var idStr = id ? "/" + id : ""
    return this.http.get(`${this.urlBase}s` + idStr)
      .catch((error: any) => Observable.throw(error || 'Server error'))
  }

  // incompleto
  // create or update
  write(myData): Observable<any> {
    let body = JSON.stringify(myData)
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' })

    if (myData._id) {
      return this.http
        .put(`${this.urlBase}s/${myData._id}`, body, { headers: headers })
    } else {
      return this.http
        .post(`${this.urlBase}s`, body, { headers: headers })
    }
  }




  getOwnLocal(): Observable<any> {
    return new Observable(observer => {
      this.dataObserver = observer
      this.dataObserver.next({});
    });
  }

  // https://angular-2-training-book.rangle.io/handout/observables/using_observables.html
  getOwn(): Observable<any> {
    return this.http
      .get(`${this.urlBase}/get-own`)
      .map((res: any) => {
        this.dataObserver.next(res);
        return res
      })

  }

  saveOwn(myData): Observable<Response> {
    let body = JSON.stringify(myData)
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' })

    return this.http
      .put(`${this.urlBase}/save-own`, body, { headers: headers })
      .map((res: any) => {
        this.dataObserver.next(res);
        return res
      })

  }


}
