import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, Events } from 'ionic-angular';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { HomePage } from '../home/home';

import { AuthProvider } from '../../providers/auth/auth';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public menuCtrl: MenuController,
    public events: Events, private fb: Facebook, private auth: AuthProvider
  ) {
    this.menuCtrl.swipeEnable(false);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  loginFacebook__() {
    const fechaInicio = new Date();
    console.log(fechaInicio);
    localStorage.setItem('username', 'Usuario invitado');
    localStorage.setItem('FechaInicio', '' + fechaInicio);
    this.navCtrl.setRoot(HomePage);
  }









    verifySession() {
      return new Promise((res,rej) =>{
        this.fb.getLoginStatus()
        .then((authData: FacebookLoginResponse) => {
          if(authData.status == "unknown"){
            rej()
            return
          }
          res(authData)
        })
        .catch(e => {
          rej()
          console.log('Error getLoginStatus', e);
        });
      })
    }


  loginFacebook() {
    this.verifySession().then((data)=>{
      console.log(111,data)
      this.updateInfo(data)
    },()=>{
      this.fb.login(['public_profile'])
        .then((authData: FacebookLoginResponse) => {
          console.log('Logged into Facebook!', authData); // accessToken, userID
          //this.fb.api("/me?fields=id,email,name,birthday,picture?redirect=0,type=large", ["email", "public_profile", "user_birthday"]).then((userData) => {
          this.fb.api("/me?fields=id,email,name,birthday,picture", []).then((userData) => {
            console.log("Result: ", userData);
            this.saveInfo(userData,authData)

          }).catch(e => console.log('Error api----', e));
        })
        .catch(e => {
          console.log('Error logging into Facebook', e);
          alert('error')
        });
    })
  }


  saveInfo(userData, authData) {
    console.log(authData, userData)
    var obj = {
      name: userData.name,
      email: userData.email,
      facebookId: userData.id,
      facebookToken: authData.authResponse.accessToken,
    }
    console.log(111,obj)
    this.auth.login(obj).subscribe(data => {
      if (data) {
        this.navCtrl.setRoot(HomePage);
        return
      }
      console.error('Error en login')
    }, err => {
      console.error(err)
    })
  }

  updateInfo(data) {
    var obj = {facebookId:data.authResponse.userID}

    this.auth.login(obj).subscribe(data => {
      if (data) {
        this.navCtrl.setRoot(HomePage);
        return
      }
      console.error('Error en login')
    }, err => {
      console.error(err)
    })
  }

}
